package rpg.monsters;

import rpg.Console;
import rpg.characters.BaseHero;

public class Matango extends Monster {
	public Matango() {

	}

	private int hp = 50;
	static final int LEVEL = 10;
	private char suffix;

	public Matango(char suffix) {
		this.suffix = suffix;
	}

	public void attack(BaseHero bh) {
		Console.disp("お化けキノコ" + this.suffix + "の攻撃", 100);
		Console.disp("10のダメージ", 100);
		bh.setHp(bh.getHp() - 10);
	}

	@Override
	public int getHp() {
		return this.hp;
	}

	@Override
	public void setHp(int hp) {
		this.hp = hp;
	}

	@Override
	public void run() {
		Console.disp("お化けキノコ" + this.suffix + "は逃げ出した！", 100);
	}

	void sleepingGas() {
		Console.disp("お化けキノコ" + this.suffix + "の催眠ガス！", 100);
		Console.disp("相手を眠らせた！", 100);
	}

}
