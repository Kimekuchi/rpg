package rpg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * RPGツクレールのコンソール周りの処理クラス
 *
 * @author yshim
 */
public class Console {
	private static Scanner scanner;

	/**
	 * 画面の切り替え
	 */
	public static void clearScreen() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (IOException a) {
			System.out.println(a);
			System.exit(1);
		} catch (InterruptedException b) {
			System.out.println(b);
			System.exit(1);
		}
	}

	/**
	 * プレイヤーからの入力を受け付けるメソッド
	 *
	 * @param msg 入力前に表示するメッセージ
	 * @return 入力文字列
	 */
	public static String input(String msg) {
		System.out.print(msg);
		scanner = new Scanner(System.in);
		return scanner.nextLine();
	}

	/**
	 * コンソールにメッセージを表示するメソッド
	 *
	 * @param msg 表示メッセージ
	 * @param time 表示速度（ミリ秒単位で設定。値が大きいほど、表示が遅くなる）
	 */
	public static void disp(String msg, int time) {
		//      dispLine("-");
		dispMsg(msg, time);
		//      dispLine("-");
	}

	/**
	 * コンソールに複数のメッセージを表示するメソッド
	 *
	 * @param msgList 表示メッセージリスト
	 * @param time 表示速度（ミリ秒単位で設定。値が大きいほど、表示が遅くなる）
	 */
	public static void disp(ArrayList<String> msgList, int time) {
		dispLine("-");
		for (String msg : msgList) {
			dispMsg(msg, time);
		}
		dispLine("-");
	}

	/**
	 * ラインを表示するプライベートメソッド
	 *
	 * @param s ライン文字列
	 */
	private static void dispLine(String s) {
		int i = 1;
		while (i <= 30) {
			System.out.print(s);
			i++;
		}
		System.out.println();
	}

	/**
	 * コンソールにメッセージを表示するプライベートメソッド<br>
	 * 指定されたミリ秒数の間、スリープ(一時的に実行を停止)させる。
	 *
	 * @param msg 表示メッセージ
	 * @param time 表示速度
	 */
	private static void dispMsg(String msg, int time) {
		for (int i = 0; i < msg.length(); i++) {
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
			}
			System.out.print(msg.charAt(i));
		}
		System.out.println();
	}

}