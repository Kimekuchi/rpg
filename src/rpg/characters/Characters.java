package rpg.characters;

import rpg.Console;
import rpg.monsters.Monster;

public abstract class Characters implements Life {
	private String name;
	private int hp;

	public void run() {
		Console.disp(this.name + "は逃げ出した", 100);
	}

	public abstract void attack(Monster m);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
}
