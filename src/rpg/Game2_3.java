package rpg;

import rpg.characters.Hero;
import rpg.characters.Wizard;

public class Game2_3 {

	public static void main(String[] args) {
		Hero satou = new Hero();
		satou.name = "サトウ";
		satou.hp = 100;

		Hero takahashi = new Hero();
		takahashi.name = "タカハシ";
		takahashi.hp = 100;

		Wizard iino = new Wizard();
		iino.heal(satou);
		iino.heal(takahashi);
		iino.heal(takahashi);

		satou.run();
		takahashi.run();


	}

}
