package rpg;
import rpg.monsters.Matango;
import rpg.monsters.Monster;
import rpg.monsters.PoisonMatango;
import rpg.monsters.Slime;

public class Game6_6 {

	public static void main(String[] args) {
		Monster[] monsters = new Monster[3];
		monsters[0] = new Slime();
		monsters[1] = new Matango();
		monsters[2] = new PoisonMatango();

		for (Monster m : monsters) {
			m.run();
		}

	}

}
