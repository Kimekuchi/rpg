package rpg;

import rpg.characters.BaseHero;
import rpg.characters.EvoHero;

public class Game4_3 {

	public static void main(String[] args) {
		BaseHero bh = new BaseHero();
		bh.run();
		EvoHero eh = new EvoHero();
		eh.run();

	}

}
