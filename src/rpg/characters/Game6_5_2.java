package rpg.characters;

public class Game6_5_2 {

	public static void main(String[] args) {
		Characters[] c = new Characters[5];
		c[0] = new BaseHero();
		c[1] = new BaseHero();
		c[2] = new Cleric("チャモロ");
		c[3] = new Wizard();
		c[4] = new Wizard();
		for (Characters ch : c) {
			ch.setHp(ch.getHp() + 50);
		}

	}

}
