package rpg.characters;

import rpg.Console;
import rpg.monsters.Monster;

public class BaseHero extends Characters {
	String name = "キャドゥ";
	private int hp = 100;

	public BaseHero() {

	}

	@Override
	public void attack(Monster m) {
		Console.disp(this.name + "の攻撃！", 100);
		m.setHp(m.getHp() - 10);
		Console.disp("１０のダメージを与えた！", 100);
	}

	@Override
	public void run() {
		Console.disp(this.name + "は逃げ出した", 100);
	}

	@Override
	public int getHp() {
		return hp;
	}

	@Override
	public void setHp(int hp) {
		this.hp = hp;
	}
}
