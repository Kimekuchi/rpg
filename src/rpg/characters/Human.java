package rpg.characters;

public interface Human extends Creature {
	void talk();

	void watch();

	void hear();

}
