package rpg.characters;

import rpg.Console;
import rpg.monsters.Matango;

public class BaseHero {
	String name = "キャドゥ";
	private int hp = 100;

	public BaseHero() {
		Console.disp("BaseHeroのコンストラクタが動作", 100);
	}

	public void attack(Matango ma) {
		Console.disp(this.name + "の攻撃！", 100);
		ma.setHp(ma.getHp() - 10);
		Console.disp("１０のダメージを与えた！", 100);
	}

	public void run() {
		Console.disp(this.name + "は逃げ出した", 100);
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
}
