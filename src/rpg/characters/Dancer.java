package rpg.characters;

import rpg.Console;
import rpg.monsters.Matango;

public class Dancer extends Characters {
	public void dance() {
		Console.disp(this.getName() + "は情熱的に踊った", 100);
	}

	@Override
	public void attack(Matango m) {
		Console.disp(this.getName() + "の攻撃", 100);
		Console.disp("敵に３ポイントのダメージをあたえた！", 100);
		m.setHp(m.getHp() - 5);

	}
}
