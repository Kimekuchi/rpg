package rpg.characters;

import rpg.weapon.Wand;

public class Wizard {

	private String name;
	private int hp;
	private int mp;
	private Wand wand;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if (name == null) { //nullが設定されそうになった場合
			throw new IllegalArgumentException("名前がnullです。処理を中断。");
		}
		if (name.length() <= 2) { //文字列長が異常（短い）の場合
			throw new IllegalArgumentException("名前が短すぎます。処理を中断。");
		}
		if (name.length() >= 8) { //文字列長が異常（長い）の場合
			throw new IllegalArgumentException("名前が長すぎです。処理を中断。");
		}
		this.name = name;
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		if (hp < 0) {
			System.out.println("値が不正です。値を0に設定します");
			this.hp = 0;
		}
		this.hp = hp;
	}

	public int getMp() {
		return this.mp;
	}

	public void setMp(int mp) {
		if (mp < 0) {
			System.out.println("値が不正です。値を0に設定します");
			this.mp = 0;
		}
		this.mp = mp;
	}

	public Wand getWand() {
		return this.wand;
	}

	public void setWand(Wand wand) {
		if (wand == null) {
			throw new IllegalArgumentException("設定がnullです。処理を中断。");
		}
		this.wand = wand;
	}

	void heal(Hero h) {
		int basePoint = 10;
		int recovPoint = (int) (basePoint * this.wand.getPower());
		h.setHp(h.getHp() + recovPoint);//勇者回復
		System.out.println(h.getName() + "のHPを" + recovPoint + "回復した！");
	}

}
