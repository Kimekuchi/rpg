package rpg;

import rpg.characters.EvoHero;
import rpg.monsters.Matango;

public class Game4_4 {

	public static void main(String[] args) {
		EvoHero eh = new EvoHero();
		eh.fly();
		Matango ma = new Matango('A');
		eh.attack(ma);

	}

}
