package rpg.monsters;

import rpg.Console;
import rpg.characters.BaseHero;

public class PoisonMatango extends Matango {
	int poison = 5;
	int damage;

	public void attack(BaseHero bh) {
		super.attack(bh);
		if (poison >= 1) {
			Console.disp("さらに毒の胞子をばらまいた！", 100);
			damage = bh.getHp() / 5;
			Console.disp(damage + "ポイントのダメージ", 100);
			bh.setHp(bh.getHp() - damage);
			poison -= poison;
		}

	}
}
