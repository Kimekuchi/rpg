package rpg.monsters;

import rpg.Console;

public class Monster {
	private int hp;

	public void run() {
		Console.disp("モンスターは逃げ出した", 100);
	}

	public int getHp() {

		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;

	}
}
