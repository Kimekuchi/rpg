package rpg;

import rpg.characters.Hero;
import rpg.monsters.Matango;

public class Game1_2 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Hero hero = new Hero();
		hero.hp = 100;
		hero.name = "サトウ";
		Console.disp("勇者サトウが誕生した", 100);

		Matango matangoA = new Matango();
		matangoA.hp = 50;
		matangoA.suffix = 'A';
		Console.disp("お化けキノコ" + matangoA.suffix + "が現れた", 100);

		Matango matangoB = new Matango();
		matangoB.hp = 40;
		matangoB.suffix = 'B';
		Console.disp("お化けキノコ" + matangoB.suffix + "が現れた", 100);

		hero.slip();
		matangoA.run();

		matangoB.run();
		hero.run();

	}

}