package rpg.characters;

import rpg.Console;
import rpg.monsters.Matango;

public class EvoHero extends BaseHero {
	private boolean flying;

	public EvoHero() {
		Console.disp("EvoHeroのコンストラクタが動作", 100);
	}

	@Override
	public void attack(Matango ma) {
		super.attack(ma);
		if (this.flying) {
			super.attack(ma);
		}
	}

	public void fly() {
		this.flying = true;
		Console.disp("飛び上がった！", 100);
	}

	public void land() {
		this.flying = false;
		Console.disp("着地した！", 100);
	}

	@Override
	public void run() {
		Console.disp("ドロンさせていただきます。", 100);
	}
}
