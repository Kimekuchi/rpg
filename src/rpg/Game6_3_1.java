package rpg;

import rpg.characters.Wizard;
import rpg.monsters.Matango;

public class Game6_3_1 {

	public static void main(String[] args) {
		Wizard w = new Wizard();
		Matango m = new Matango('A');
		w.setName("CADO");
		w.attack(m);
		w.fireball(m);
	}

}
