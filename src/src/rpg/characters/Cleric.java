package rpg.characters;

public class Cleric {
	String name;
	int hp;
	static final int MAX_HP = 50;
	int mp;
	static final int MAX_MP = 10;

	Cleric(String name) {
		this(name, MAX_HP, MAX_MP);
	}

	Cleric(String name, int hp) {
		this(name, hp, MAX_MP);
	}

	Cleric(String name, int hp, int mp) {
		this.name = name;
		this.hp = hp;
		this.mp = mp;
	}

	void selfAid() {
		mp = -5; //mpを5消費
		hp = MAX_HP; //hpを全回復

	}

	int pray(int sec) {
		sec = sec + (int) (Math.random() * 3); //回復量を計算
		if (mp + sec > MAX_MP) { //MP判定
			sec = MAX_MP - mp;
		}
		return sec; //MPを返す
		/**
		 * 秒数に応じてMP回復
		 *
		 * @param src 祈りの秒数
		 * @return 戻り値sec 回復量を戻す
		 */
	}

}
