package rpg.characters;

import rpg.Console;
import rpg.weapon.Sword;

public class Hero {

	private String name; //名前
	private int hp; //HP
	Sword sword; //勇者が装備している剣の情報
	static int money;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if (name == null) { //nullが設定されそうになった場合
			throw new IllegalArgumentException("名前がnullです。処理を中断。");
		}
		if (name.length() <= 1) { //文字列長が異常（短い）の場合
			throw new IllegalArgumentException("名前が短すぎます。処理を中断。");
		}
		if (name.length() >= 8) { //文字列長が異常（長い）の場合
			throw new IllegalArgumentException("名前が長すぎです。処理を中断。");
		}
		this.name = name; //検査完了
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public Hero() {
		this("ダミー");
	}

	public Hero(String name) {
		this.hp = 100;
		this.name = name;
	}

	void bye() {
		Console.disp("勇者は別れを告げた", 100);
	}

	private void die() {
		Console.disp(this.name + "は死んでしまった!", 100);
		Console.disp("GAMEOVERです。", 100);
	}

	/* void attack(Matango m) { Console.disp(this.name + "の攻撃", 100); Console.disp("お化けキノコ" + m.suffix + "に" + sword.damage + "ポイントのダメージを与えた", 100); m.hp -= sword.damage; if (m.hp <= 0) { Console.disp("お化けきのこ" + m.suffix + "を倒した", 100); } else { Console.disp("お化けキノコ" + m.suffix + "から2ポイントの反撃を受けた", 100); } this.hp -= 2; if (this.hp <= 0) { this.die(); } } */

	void run() {
		Console.disp(name + "は逃げ出した！", 100);
		Console.disp("GAME OVER", 100);
		Console.disp("最終HPは" + hp + "でした。", 100);
	}

	public void sleep() {
		Console.disp(name + "は眠った！", 100);
		Console.disp("HPが100になった。", 100);
	}

	void sit(int sec) {
		hp = hp + sec;
		Console.disp(name + "は、" + sec + "秒座った！", 100);
		Console.disp("HPが" + sec + "ポイント回復した。", 100);
	}

	void slip() {
		hp = hp - 5;
		Console.disp(name + "は、転んだ！", 100);
		Console.disp("5のダメージ。", 100);
	}

	/**
	 * static void setRandomMoney() { Hero.money = (int) (Math.random() * 1000); Console.disp(this.name + "たちの所持金を初期化しました", 100); }
	 */
}
