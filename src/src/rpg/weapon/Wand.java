package rpg.weapon;

public class Wand {
	private String name;
	private double power;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if (name == null) { //nullが設定されそうになった場合
			throw new IllegalArgumentException("名前がnullです。処理を中断。");
		}
		if (name.length() <= 2) { //文字列長が異常（短い）の場合
			throw new IllegalArgumentException("名前が短すぎます。処理を中断。");
		}
		if (name.length() >= 8) { //文字列長が異常（長い）の場合
			throw new IllegalArgumentException("名前が長すぎです。処理を中断。");
		}
		this.name = name;
	}

	public double getPower() {
		return this.power;
	}

	public void setPower(double power) {
		if (power >= 0.5) { //nullが設定されそうになった場合
			throw new IllegalArgumentException("名前がnullです。処理を中断。");
		}
		if (power <= 100.0) { //nullが設定されそうになった場合
			throw new IllegalArgumentException("名前がnullです。処理を中断。");
		}
		this.power = power;
	}
}
