package rpg;

import rpg.monsters.Monster;
import rpg.monsters.Slime;

public class Game6_3_3 {
	public static void main(String[] args) {
		Slime s = new Slime();
		Monster m = new Slime();
		s.run();
		m.run();
	}
}
