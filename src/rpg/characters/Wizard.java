package rpg.characters;

import rpg.Console;
import rpg.monsters.Matango;
import rpg.weapon.Wand;

public class Wizard extends Characters {

	private int mp;
	private Wand wand;

	public int getMp() {
		return this.mp;
	}

	public void setMp(int mp) {
		if (mp < 0) {
			System.out.println("値が不正です。値を0に設定します");
			this.mp = 0;
		}
		this.mp = mp;
	}

	public Wand getWand() {
		return this.wand;
	}

	public void setWand(Wand wand) {
		if (wand == null) {
			throw new IllegalArgumentException("設定がnullです。処理を中断。");
		}
		this.wand = wand;
	}

	void heal(Hero h) {
		int basePoint = 10;
		int recovPoint = (int) (basePoint * this.wand.getPower());
		h.setHp(h.getHp() + recovPoint);//勇者回復
		System.out.println(h.getName() + "のHPを" + recovPoint + "回復した！");
	}

	@Override
	public void attack(Matango m) {
		Console.disp(this.getName() + "の攻撃", 100);
		Console.disp("敵に3ポイントのダメージ", 100);
		m.setHp(m.getHp() - 3);
	}

	public void fireball(Matango m) {
		Console.disp(this.getName() + "は火の玉を放った！", 100);
		Console.disp("敵に20ポイントのダメージ", 100);
		this.mp -= 5;
		m.setHp(m.getHp() - 20);
	}
}
