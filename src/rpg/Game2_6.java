package rpg;

import rpg.characters.Hero;

public class Game2_6 {

	public static void main(String[] args) {
		Hero.money = 100;
		Hero h1 = new Hero();
		Hero h2 = new Hero();
		h1.money = 300;
		Console.disp(Hero.money + "\t" + h1.money + "\t" + h2.money, 100);
	}

}
