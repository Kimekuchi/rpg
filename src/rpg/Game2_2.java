package rpg;

import rpg.characters.Hero;
import rpg.weapon.Sword;

public class Game2_2 {

	public static void main(String[] args) {
		Sword sword = new Sword();
		sword.name = "炎の剣";
		sword.damage = 10 ;
		Hero hero = new Hero();
		hero.hp = 100;
		hero.name = "サトウ";
		hero.sword = sword;
		Console.disp("勇者" + hero.name + "の武器は、" + hero.sword.name + "です" ,100);
	}

}
