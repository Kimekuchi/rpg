package rpg;

import rpg.characters.Characters;
import rpg.characters.Wizard;
import rpg.monsters.Matango;

public class Game6_3_2 {

	public static void main(String[] args) {
		Characters c = new Wizard();
		Matango m = new Matango('A');
		c.setName("CADO");
		if (c instanceof Wizard) {
			Wizard w = (Wizard) c;
			w.setName("CADO");
			w.attack(m);
			w.fireball(m);
		}
	}

}
